<?php
    //Associative_array
    //Example:1

   $student=array("name"=>"Resma","batch"=>"32","roll"=>"51");
   echo "$student[name]";
   echo "<br>";
   var_dump($student);
   echo "<br>";
   
   //Example:2
   $run=array("Sakib"=>"100","Tamim"=>"102","Musfiq"=>"99");
   foreach($run as $batsman_name => $batsman_run) {
     echo $batsman_name .$batsman_run;
     echo "<br>";
   }
   
   //Example:3
    
    $month=[
        1 => "jun",
        2 => "feb",
        3 => "mar"
       ];
   var_dump($month);
    echo "<br>";
    
   //Example:4
    
    $array=array(3=>"x",11 => "y",5=>"z","w");
    var_dump($array);
    echo "<br>";
    
   //Example:5
    $age=array("rahim"=>"30","sakib"=>"25");
    echo "peter is :".$age["rahim"]." years old";
   
   
?>
   